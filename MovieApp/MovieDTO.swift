//
//  MovieDTO.swift
//  Web Service
//
//  Created by Omara on 03.11.18.
//  Copyright © 2018 Mahmoud Omara. All rights reserved.
//

import Foundation
import ObjectMapper

class MovieDTO : Mappable{
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        imagePath <- map["poster_path"]
        productionYear <- map["release_date"]
        rate <- map["vote_average"]
        movieID <- map["id"]
        
    }
    
    
    var title: String?
    var imagePath: String?
    var productionYear: String?
    var rate: Double?
     var movieID : Int?
    
    
    
}
