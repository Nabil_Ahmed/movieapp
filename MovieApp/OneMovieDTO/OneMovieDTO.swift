//
//  OneMovieDTO.swift
//  MovieApp
//
//  Created by Nabil on 11/15/18.
//  Copyright © 2018 Nabil. All rights reserved.
//


import Foundation
import ObjectMapper

class OneMovieDTO : Mappable{
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        movieName <- map["original_title"]
        overview <- map["overview"]
        imageURL <- map["poster_path"]
     
        
    }
    
    
    var movieName: String?
    var overview: String?
    var imageURL: String?
   
    
    
    
}
