//
//  DiscoverOneMovieResultDTO.swift
//  MovieApp
//
//  Created by Nabil on 11/15/18.
//  Copyright © 2018 Nabil. All rights reserved.
//

import Foundation
import ObjectMapper

class OneDiscoverOneMovieResultsDTO: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mymovies <- map["results"]
    }
    
    var mymovies: [OneMovieDTO]?
    
    
}
