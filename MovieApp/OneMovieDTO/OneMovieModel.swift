//
//  OneMovieModel.swift
//  MovieApp
//
//  Created by Nabil on 11/15/18.
//  Copyright © 2018 Nabil. All rights reserved.
//

import Foundation
class OneMovieModel {
    
    var movieName: String
    var overview: String
    var imageURL: String
  
    
    init(movieName: String, overview: String, imageURL: String) {
        self.movieName = movieName
        self.overview = overview
        self.imageURL = imageURL
    
}
}
