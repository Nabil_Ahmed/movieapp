//
//  MovieDetailsViewController.swift
//  MovieApp
//
//  Created by Nabil on 11/12/18.
//  Copyright © 2018 Nabil. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper
import ObjectMapper
import MaterialActivityIndicator




class MovieDetailsViewController: UIViewController {

    private let indicator = MaterialActivityIndicatorView()

    var id: Int!
    @IBOutlet weak var textOverview: UILabel!
    @IBOutlet weak var textTitle: UILabel!
    @IBOutlet weak var imageHeader: UIImageView!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        indicator.startAnimating()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        indicator.stopAnimating()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityIndicatorView()

        
        let method: HTTPMethod = .get
        
        
        let completeURl = "https://api.themoviedb.org/3/movie/\(id!)?api_key=6f5b3e7e18384e6380b996f9d745b3ee"
        WebService.request(completeURl: completeURl, method: method, params: nil, headers: nil, onSucces: {
            (result: OneMovieDTO) in
            self.textTitle.text = result.movieName
            self.textOverview.text = result.overview
            let url = "https://image.tmdb.org/t/p/w500"+(result.imageURL)!
            
            let remoteImageURL = URL(string: url)!
            Alamofire.request(remoteImageURL).response { (response) in
                if response.error == nil {
                    
                }
                if let data = response.data {
                    self.imageHeader.image = UIImage(data : data)
                }
            }

            
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}

extension MovieDetailsViewController {
    private func setupActivityIndicatorView() {
        view.addSubview(indicator)
        setupActivityIndicatorViewConstraints()
    }
    
    private func setupActivityIndicatorViewConstraints() {
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
