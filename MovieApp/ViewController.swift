//
//  ViewController.swift
//  MovieApp
//
//  Created by Nabil on 11/8/18.
//  Copyright © 2018 Nabil. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper
import ObjectMapper

class ViewController: UIViewController {

      var movies:[MovieDTO] = []
  //  var temp :  [String] = ["e","w","s","d","d"]
    @IBOutlet weak var collectionTable: UICollectionView!
      
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let baseURL = "https://api.themoviedb.org/3"
        let funcName = "/discover/movie"
        let method: HTTPMethod = .get
        let apiKey = "ceb888b71023afda704f84975d2642b5"
        let page = 1
        
        let completeURl = "\(baseURL)\(funcName)?api_key=\(apiKey)&page=\(page)"
        
        WebService.request(completeURl: completeURl, method: method, params: nil, headers: nil, onSucces: {
            (result: DiscoverMovieResultsDTO) in
            self.movies = result.movies ?? []
            self.collectionTable.reloadData()
        })
        
        
    }

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let dest = segue.destination as! MovieDetailsViewController
//        dest.id = (sender as! Int)
//    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as! MovieDetailsViewController
      
        
        dest.id =  movies[collectionTable.indexPathsForSelectedItems![0].row].movieID
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}

extension ViewController : UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
    
    //    print(movies[indexPath.item].movieID ?? "errorID")
        let indxyear = movies[indexPath.item].productionYear?.components(separatedBy: "-").first
        
        cell.textYear.text = indxyear
        cell.textRate.text = String(format: "%.2f", movies[indexPath.item].rate!)
        cell.textTitle.text = movies[indexPath.item].title

        let url = "https://image.tmdb.org/t/p/w500"+(movies[indexPath.item].imagePath)!
        
         let remoteImageURL = URL(string: url)!
         Alamofire.request(remoteImageURL).response { (response) in
            if response.error == nil {
               
            }
            if let data = response.data {
                cell.imageMovie.image = UIImage(data : data)
            }
        }
      
        
        
        return cell
    }
  
    
}


extension ViewController : UICollectionViewDelegate
{
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if movies[indexPath.item].movieID != nil {
//         //   print("selected Movie id: \(movies[indexPath.row].movieID!)")
//            performSegue(withIdentifier: "showMovieDetails", sender: movies[indexPath.item].movieID!)
//        }
//    }
}
