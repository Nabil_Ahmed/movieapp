//
//  MovieModel.swift
//  Web Service
//
//  Created by Omara on 03.11.18.
//  Copyright © 2018 Mahmoud Omara. All rights reserved.
//

import Foundation

class MovieModel {
    
    var title: String
    var imagePath: String
    var productionYear: String
    var rate: Double
    var movieID : Int
    
    init(title: String, imagePath: String, productionYear: String, rate: Double , movieID :Int) {
        self.title = title
        self.imagePath = imagePath
        self.productionYear = productionYear
        self.rate = rate
        self.movieID = movieID
    }
    
}
