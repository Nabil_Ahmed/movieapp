//
//  CollectionViewCell.swift
//  MovieApp
//
//  Created by Nabil on 11/8/18.
//  Copyright © 2018 Nabil. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var textTitle: UILabel!
    @IBOutlet weak var textRate: UILabel!
    @IBOutlet weak var textYear: UILabel!
    @IBOutlet weak var imageMovie: UIImageView!
    
}
