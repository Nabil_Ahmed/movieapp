//
//  DiscoverMovieResultsDTO.swift
//  Web Service
//
//  Created by Omara on 03.11.18.
//  Copyright © 2018 Mahmoud Omara. All rights reserved.
//

import Foundation
import ObjectMapper

class DiscoverMovieResultsDTO: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        movies <- map["results"]
    }
    
    var movies: [MovieDTO]?
    
    
}
